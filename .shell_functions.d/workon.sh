#!/bin/bash


# Use, eg 
#   $> workon cfs
# to quickly start working on a project

RCDIR=$HOME/.workonrc



function workon() 
{
	# Clean up environment, in case of switching projects
	for ITEM in $__WORKON_VARS; do
		echo 'item:  '$ITEM
		unset $ITEM 2>/dev/null
	done	
	for ITEM in $__WORKON_ALIASES; do
		unalias $ITEM 2>/dev/null
	done	
	for ITEM in $__WORKON_FNS; do
		unset -f $ITEM 2>/dev/null
	done	

  case $1 in 
    cfs )
			__WORKON_CFG=$RCDIR/cfs
			__WORKON_VARS="$(cat $__WORKON_CFG | sed -n -r '/^[ \t]*[a-zA-Z0-9_\-]+=/{s/=.*//;p}'  )"
			__WORKON_ALIASES="$(cat $__WORKON_CFG | sed -n -r '/^[ \t]*alias /{s/^[ \t]*alias //; s/=.*//;p}'  )"
			__WORKON_FNS="$(cat $__WORKON_CFG | sed -n -r '/^[ \t]*function[ |\(]/p')"

			# echo "$__WORKON_FNS"
			# echo "$__WORKON_ALIASES";
			# echo "$__WORKON_VARS";
			source $__WORKON_CFG
			;;
      

    cfs-pan )
      
      # Move to working directory
      cd /var/www/cfs-pan

      # # Activate drush alias
      # drush use zen5.chessforsuccess.org

      # Pantheon db
      MYSQLCXN="-h dbserver.dev.46db633f-00a1-482f-85aa-faaf1e438ed6.drush.in -u pantheon -pc9e5a664cdd4449fb35e44d27413c9d6 -P 11531 pantheon"
      alias mysql="/usr/bin/mysql $MYSQLCXN"
      alias mysqlE="/usr/bin/mysql $MYSQLCXN -e"
      alias mysqlD="/usr/bin/mysqldump $MYSQLCXN"

      # Dev-vm db
      MYSQLCXN2="-u root -pmcse4321 cfs_pantheon"
      alias mysql2="/usr/bin/mysql $MYSQLCXN2"
      alias mysqlE2="/usr/bin/mysql $MYSQLCXN2 -e"
      alias mysqlD2="/usr/bin/mysqldump $MYSQLCXN2"
      
      terminus auth login subscriber@onstance.com --password=mcse4321
      alias t="terminus --site=cfs"
      alias gt="t git"
      alias dt="t drush"


      ;;

    cfscom )

      # Move to working directory
      cd /var/www/cfscommercedemo

      # Activate drush alias
      drush use 

      ;; 

    lms )

      # Move to working directory
      cd /var/www/lms

      # Activate drush alias
      #drush use 

      ;; 

    poo )

      # Move to working directory
      cd /var/www/poosh

      # Activate drush alias
      #drush use 

      ;; 

    fuj )

      # Move to working directory
      cd /var/www/fujimi

      alias mysql="mysql -u root -pmcse4321 fujimi_train"
      terminus auth login subscriber@onstance.com --password=mcse4321
      alias t="terminus --site=fujimi-training"
      alias gt="t git"
      alias dt="t drush"

      alias t="terminus --site=fujimi-training"
      alias gt="t git"
      alias dt="t drush"

      alias t="terminus --site=fujimi-training"
      alias gt="t git"
      alias dt="t drush"

      alias t="terminus --site=fujimi-training"
      alias gt="t git"
      alias dt="t drush"
      # Activate drush alias
      #drush use 

      ;; 

    onspm )

      WORKING_DIR=/var/www/onspm
      TERMINUS_USER="subscriber@onstance.com"
      TERMINUS_PASS="mcse4321"
      TERMINUS_SITE="onspm"
      DB1_HOST="dbserver.dev.210e4afb-e7f1-4570-a5cf-88bbf43acd31.drush.in"
      DB1_USER="pantheon"
      DB1_PASS="ed547930eb78494eb3ee514dee6fa407"
      DB1_PORT="11477"
      DB1_SCHEMA="pantheon"
      DB2_HOST="localhost"
      DB2_USER="root"
      DB2_PASS="mcse4321"
      DB2_PORT=""
      DB2_SCHEMA="onspm"

      SSHFS_USER="dev.210e4afb-e7f1-4570-a5cf-88bbf43acd31"
      SSHFS_HOST="appserver.dev.210e4afb-e7f1-4570-a5cf-88bbf43acd31.drush.in"
      SSHFS_DIR="/srv/bindings/551d33f60eca4da3ac28a2ca1ef11c84/code"
      SSHFS_OPTS="-p2222"
      SSHFS_MOUNTPOINT="/onspm-pan"

      # ------------------------------------

      cd "$WORKING_DIR"

      # compile db connection defs
      MYSQLCXN="-h ""$DB1_HOST"" -u ""$DB1_USER"" -p""$DB1_PASS"" -P ""$DB1_PORT"" ""$DB1_SCHEMA"
      MYSQLCXN2="-h ""$DB2_HOST"" -u ""$DB2_USER"" -p""$DB2_PASS"" -P ""$DB2_PORT"" ""$DB2_SCHEMA"
      # remove -P port param if not used
      if [ -z "$DB1_PORT" ]; then MYSQLCXN=$(echo ${MYSQLCXN/-P /}); fi
      if [ -z "$DB2_PORT" ]; then MYSQLCXN2=$(echo ${MYSQLCXN2/-P /}); fi

      # make aliases
      ALIASES=(t="terminus --site=""$TERMINUS_SITE")
      ALIASES+=(gt="t git")
      ALIASES+=(dt="t drush")
      ALIASES+=(d="drush")
      ALIASES+=(db="/usr/bin/mysql $MYSQLCXN")
      ALIASES+=(dbe="/usr/bin/mysql $MYSQLCXN -e")
      ALIASES+=(dbd="/usr/bin/mysqldump $MYSQLCXN")
      ALIASES+=(db2="/usr/bin/mysql $MYSQLCXN2")
      ALIASES+=(db2e="/usr/bin/mysql $MYSQLCXN2 -e")
      ALIASES+=(db2d="/usr/bin/mysqldump $MYSQLCXN2")

      make_aliases

      # log into Pantheon terminal
      if [ -n "$TERMINUS_USER" ]; then 
        terminus auth login "$TERMINUS_USER" --password="$TERMINUS_PASS"
      fi


      # mount sshfs connection
      if [ ! -d $SSHFS_MOUNTPOINT ]; then 
        mkdir "$SSHFS_MOUNTPOINT"
      fi
      if ! mountpoint -q -- $SSHFS_MOUNTPOINT; then
        sshfs "$SSHFS_USER"@"$SSHFS_HOST":"$SSHFS_DIR" "$SSHFS_MOUNTPOINT" "$SSHFS_OPTS"
      fi

      echo 'Mountpoint for pantheon code files: '$SSHFS_MOUNTPOINT

      # define functions
      function upl () { 
        if ! mountpoint -q -- $SSHFS_MOUNTPOINT; then
          sshfs "$SSHFS_USER"@"$SSHFS_HOST":"$SSHFS_DIR" "$SSHFS_MOUNTPOINT" "$SSHFS_OPTS"
        fi
        SRC="$(readlink -f "$1")"; 
        TGT=$(echo "$SRC" | sed 's%'$SSHFS_LOCAL_ROOT'%%'); 
        TGT=/onspm-pan/"$TGT"; 
        cp "$SRC" "$TGT"
      }

      echo "To upload a (single) local file (from within working directory ($WORKING_DIR), use 'upl {file}'"
      export upl

      ;; 
    *)
      echo 'Options'
      echo ' +  cfs [-nos | -stop]'
      echo ' +  cfs-pan'
      echo ' +  cfscom'
      echo ' +  lms'
      echo ' +  poo'
      echo ' +  fuj'
      echo ' +  onspm'
      ;;
      
  esac
}



function make_aliases() {
  ct=${#ALIASES[@]}
  ALIASES_REPORT=""
  echo Creating aliases:
  for i in $(seq 0 $(($ct - 1))); do 
    ALIAS_NAME=$(echo ${ALIASES[$i]} | sed -e 's/=.*//')
    ALIAS_COMMAND=$(echo ${ALIASES[$i]} | sed -e 's/[^=]*=//')
    alias "$ALIAS_NAME"="$ALIAS_COMMAND"
    ALIASES_REPORT="$ALIASES_REPORT""$(printf "%s   \t%s" "$ALIAS_NAME" "$ALIAS_COMMAND")""\n"
  done
  echo -e "$ALIASES_REPORT"
}


# Utility function to start a compass/sass watch polling process in background.
# Detects if such a process is already in place, in which case it does not
# spawn a duplicate.
function sassbkgwatch() {
  
  SASSDIR=$1
  STOP_WATCH=
  if [ ! -z $2 ]; then
    case $2 in
      '-stop' )
        STOP_WATCH=1
        ;;
    esac
  fi

  PROCS=$(ps -eo pid,cmd | grep compass | grep -v '^[ 0-9]\+ grep')

  IFS=$'\n'
  WATCHEXISTS=FALSE
  for ITEM in $PROCS; do 
    PID=$(echo "$ITEM" | sed -e 's/ .*//')
    DIR=$(echo "$ITEM" | sed -e 's/^.* watch //')
    # Handle cases where command run with relative path
    if [ ${DIR:0:1} != '/' ] ; then 
      ROOTDIR=$(pwdx $PID | sed -e 's/^.* //')
      SEDCMD='s#^'$ROOTDIR'##'
      DIR=$(echo $DIR | sed -e $SEDCMD)
      DIR=$ROOTDIR/$(echo $DIR | sed -e 's#^/##')
    fi
    if [ "$DIR" == "$SASSDIR" ]; then
      WATCHEXISTS=TRUE
      break
    fi
  done
  if [ "$WATCHEXISTS" == "FALSE" ]; then
    compass watch "$SASSDIR" 2> /dev/null &
    # # with redirect of output 
    # compass watch "$SASSDIR" &>/var/log/compasswatch &
  else
    echo STOP_WATCH: "$STOP_WATCH"
    if [ ! -z $STOP_WATCH ]; then
      if [ ! -z $PROCS ]; then
        for ITEM in $PROCS; do 
          PID=$(echo "$ITEM" | sed -e 's/^[ ]*//;s/ .*//')
          kill $PID &> /dev/null
          echo "Killed compass watch"
          echo "  $ITEM"
        done
      else
        echo "NO COMPASS WATCH FOUND"
      fi
    else
      echo "COMPASS WATCH ALREADY IN PLACE"
    fi
  fi
}


# echo &1



