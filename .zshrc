autoload -U promptinit
promptinit
prompt="%S%n%s@%M %F{red}%~%f > " 

# Named directories for shorter paths in prompts
cfsroot=/var/www/cfs
cfstheme=/var/www/cfs/sites/zen5.chessforsuccess.org/themes/cfsnew
cfsmods=/var/www/cfs/sites/all/modules

: ~cfsroot ~cfstheme ~cfsmods

run_scripts()
{
    for script in $1/*; do
        [ -x "$script" ] || continue
        . $script
	echo $script
    done
}

# run_scripts $HOME/.bashrc.d
run_scripts $HOME/.shell_functions.d

