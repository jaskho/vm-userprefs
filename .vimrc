syntax on

set modeline
set ls=2
set number

" command! TT s/^/#/

" Cmt: Comment out (# style) lines in range
command! -range Cmt <line1>,<line2>s/^\([ \t]*\)\([^ \t]\)/\1# \2/

" Ucmt: Uncomment (# style) lines in range
command! -range Ucmt <line1>,<line2>s/^\([ \t]*\)# /\1/

" Vundle
set nocompatible              " be iMproved
filetype off                  " required!
set rtp+=~/.vim/bundle/vundle/
call vundle#rc()

" let Vundle manage Vundle
" required! 
Bundle 'gmarik/vundle'

" My bundles here:
"
"     " original repos on GitHub
"Bundle 'Valloric/YouCompleteMe'
Bundle 'altercation/vim-colors-solarized'
Bundle 'http://github.com/xolox/vim-misc'
Bundle 'http://github.com/xolox/vim-session'
"     Bundle 'tpope/vim-fugitive'
"     Bundle 'Lokaltog/vim-easymotion'
"     Bundle 'rstacruz/sparkup', {'rtp': 'vim/'}
"     Bundle 'tpope/vim-rails.git'
"     " vim-scripts repos
"     Bundle 'L9'
"     Bundle 'FuzzyFinder'
"     " non-GitHub repos
"     Bundle 'git://git.wincent.com/command-t.git'
"     " Git repos on your local machine (i.e. when working on your own plugin)
"     Bundle 'file:///Users/gmarik/path/to/plugin'

"     " ...
Bundle 'AnsiEsc'


filetype plugin indent on     " required!
"colorscheme elflord
"colorscheme solarized
colorscheme default


"
" Brief help
" :BundleList          - list configured bundles
" :BundleInstall(!)    - install (update) bundles
" :BundleSearch(!) foo - search (or refresh cache first) for foo
" :BundleClean(!)      - confirm (or auto-approve) removal of unused bundles
"
" see :h vundle for more details or wiki for FAQ
" NOTE: comments after Bundle commands are not allowed.

" Turn off bell/flash
set vb
set t_vb=



" *******************  Custom mappings (general)  ****************
" Map jk' to <esc> for easy exit from insert mode
inoremap jk <esc>
nnoremap gb :ls<CR>:b<Space>
nnoremap gh :b#<CR>



" *******************  Projects/Sessions  ****************
" Map \6 to show project 'pane'
nmap <Leader>6 <Plug>ToggleProject

" Tell vim-sessions plugin to save certain global variables
let g:session_persist_globals = ['&sessionoptions']
call add(g:session_persist_globals, 'g:PROJECT_PATH')
" Function to load project
"  Open vim like this:
"  vim -c "call LOADSESS('<sess/proj name>')"
function! LOADSESS(SESS)
	let g:PROJKEY = a:SESS
	exe "OpenSession"a:SESS
	let g:PROJPATH = '~/.vim/projects/'
	let g:PROJPATH .= a:SESS
	echo g:PROJPATH
	exe "Project "g:PROJPATH
	exe "normal zO"
endfunction
" Key mapping for closing/saving session
"  (Leaving project explorer window open messes up window
"   layout when session restored)
nmap Q :execute "bw .vim/projects"<CR>:qa



" *******************  Utility Functions   ****************

" Paste (or whatever) output of command
"  Usage:
"  eg, to capture output of :let
"   <in INSERT mode>
"   "=Exec('ls')p
"   # "      Start register selection
"   # =      Select command register ('=')
"   # Exec   Call this function
"   # 'let'  Put command in quotes
"   # p      Insert-mode paste()
function! Exec(command)
	redir => output
	silent exec a.command
	redir END
	return output
endfunction	
